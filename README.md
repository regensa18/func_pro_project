run front end : `npm run start`

## Instalasi dan development :
- [Panduan Backend](https://gitlab.com/regensa18/func_pro_project/blob/master/backend-haskell/README.md)
- [Panduan Frontend](https://gitlab.com/regensa18/func_pro_project/blob/master/credit_sim_fe/README.md)

## Script Automatic Deployment :
- [.gitlab-ci.yml](https://gitlab.com/regensa18/func_pro_project/blob/master/.gitlab-ci.yml) : automatic deployment ketika berada pada branch production

## Frontend :
- dapat diakses di http://credit-simulation-fe.herokuapp.com

## Backend :
 - dapat diakses di backend-haskell.herokuapp.com
 - hanya 1 endpoint
 - http://backend-haskell.herokuapp.com/motor?gaji=valueGaji&biayaHidup=valueBiayaHidup&jenis=valueJenis&maxHarga=valueMaxHarga&dp=valueDP&_accept=application/json
 - setiap value diisi sesuai keinginan
 - parameter `gaji` dan `_accept` wajib diisi

## Google Slide :
- https://docs.google.com/presentation/d/1MBsLn6OXEMDwI9aBC2bA-AMc8eZY3kk01QlH--xVpPg/edit?usp=sharing
