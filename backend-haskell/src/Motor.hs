{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE QuasiQuotes       #-}
module Motor where

import Foundation
import Yesod.Core
import ProcessCSV
import Control.Exception (IOException, try)
import Control.Monad (when)
import Data.Text (unpack)

data Error = Error {
    status :: String }
  deriving (Show)

data RecommendedMotor = RecommendedMotor {
    motor   :: Motor,
    bulan   :: Double,
    cicilan :: Double}
  deriving (Show)

instance ToJSON Motor where
    toJSON Motor {..} = object
        [ "nama"  .= nama
        , "harga" .= harga
        , "jenis" .= jenis
        ]

instance ToJSON Error where
    toJSON Error {..} = object
        [ "status" .= status
        ]

instance ToJSON RecommendedMotor where
    toJSON RecommendedMotor {..} = object
        [ "motor"   .= motor
        , "bulan"   .= bulan
        , "cicilan" .= cicilan
        ]

merge arrx []                            = arrx
merge [] arry                            = arry
merge arrx@(x:xs) arry@(y:ys) 
  | (bulan x < bulan y)  = x : merge xs arry
  | (bulan x > bulan y)  = y : merge arrx ys
  | (cicilan x < cicilan y) = x : merge xs arry
  | (cicilan x > cicilan y)  = y : merge arrx ys
  | (harga (motor x) <= harga (motor y)) = x : merge xs arry
  | (harga (motor x) > harga (motor y)) = y : merge arrx ys

mergeSort []  = []
mergeSort [x] = [x]
mergeSort xs  = merge (mergeSort(take g xs)) (mergeSort(drop g xs))
    where g = div (length xs) 2

allMotor :: IO [Motor]
allMotor = loadMotorFromCSV

filterBy criteria []      = []
filterBy criteria (x:xs)  | criteria x  = x : filterBy criteria xs
                          | otherwise   = filterBy criteria xs

filterByJenisMotor  jenis_    = filterBy (\x -> jenis x == jenis_)
filterByMaxHarga    maxHarga  = filterBy (\x -> harga x <= maxHarga)

roundBulan calculated
  | ceiledValue <= 0  = 0
  | ceiledValue <= 12 = 12
  | ceiledValue <= 24 = 24
  | otherwise         = 36
    where ceiledValue = ceiling calculated

kalkulasiBulan [] gaji biayaHidup dp = []
kalkulasiBulan (x:xs) gaji biayaHidup dp = 
  (RecommendedMotor x totalBulan perBulan) : kalkulasiBulan xs gaji biayaHidup dp
    where
          bebanHarga  = harga x - dp
          kesanggupan = gaji - biayaHidup
          totalBulan  = roundBulan (bebanHarga / kesanggupan)
          perBulan    = hitungCicilan bebanHarga totalBulan

hitungCicilan x y = (x / y) * 1.0361

getMotorR :: Handler TypedContent
getMotorR = do
        jenisMaybe      <- lookupGetParam "jenis"
        maxHargaMaybe   <- lookupGetParam "maxHarga"
        gajiMaybe       <- lookupGetParam "gaji"
        biayaHidupMaybe <- lookupGetParam "biayaHidup"
        dpMaybe         <- lookupGetParam "dp"

        let gaji            = case gajiMaybe of
                                Just gajiValue       -> read (unpack gajiValue) :: Double
        let biayaHidup      = case biayaHidupMaybe of
                                Just biayaHidupValue -> read (unpack biayaHidupValue) :: Double
                                Nothing              -> 0
        let dp              = case dpMaybe of
                                Just dpValue         -> read (unpack dpValue) :: Double
                                Nothing              -> 0
        let filterMotorFst  = case jenisMaybe of
                                Just jenis           -> filterByJenisMotor (unpack jenis)
                                Nothing              -> id
        let filterMotorSnd   = case maxHargaMaybe of
                                Just maxHarga        -> filterByMaxHarga (read (unpack maxHarga) :: Double)
                                Nothing              -> id

        edata <- liftIO $ try $ allMotor
        case edata :: Either IOException [Motor] of
          Left e -> selectRep $ do
              provideJson $ toJSON $ Error "error"
          Right motors -> selectRep $ do
              let rekomendasiMotor  = mergeSort(kalkulasiBulan ((filterMotorSnd . filterMotorFst) motors) gaji biayaHidup dp)
              let jsonObjects       = map toJSON rekomendasiMotor
              provideJson jsonObjects
