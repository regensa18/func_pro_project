{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module ProcessCSV where

import Foundation
import Yesod.Core
import Data.List.Split
import Text.CSV

data Motor = Motor {
    nama  :: String,
    harga :: Double,
    jenis :: String }
  deriving (Show)

process []      = []
process (x:xs)  = (Motor (x !! 0) (read (x !! 1) :: Double) (x !! 2)) : process xs

loadMotorFromCSV :: IO [Motor]
loadMotorFromCSV = do
    test_csv <- parseCSVFromFile "data.csv"
    case test_csv of
        Right csv -> return (process csv)
        Left err -> return [Motor "" 0 ""]
