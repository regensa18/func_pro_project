import React from 'react';
import './button.css';

const Button = (props) => {
    return(
        <button className="btn default primary" type={props.type} onClick={props.handler}>
            {props.text}
        </button> 
    );
}

export default Button;

