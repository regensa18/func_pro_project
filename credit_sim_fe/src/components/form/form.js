import React, { Component, useState, useEffect } from 'react';
import Button from '../button/button'
import './form.css';

const Form = (props) => {
  var [gaji, setGaji]             = useState('');
  var [biayaHidup, setBiayaHidup] = useState('');
  var [jenis, setJenis]           = useState('');
  var [maxHarga, setMaxHarga]     = useState('');
  var [dp, setDp]                 = useState('');

  return (
    <form className = 'form'>
      <h1>CiMor</h1>
      <h6>Cicilan Motor</h6>

      {/* Put the form inputs here */}

      <div className="row align-items-center">
        <div className="col-md-5 label-container"><p className="label">Gaji:</p></div>
        <div className="col-md-7">
          <input type='text' id='gaji' onChange={(e) => setGaji(e.target.value)}/>
        </div>
      </div>

      <div className="row align-items-center">
        <div className="col-md-5 label-container"><p className="label">Biaya hidup:</p></div>
        <div className="col-md-7">
          <input type='text' id='biayaHidup' onChange={(e) => setBiayaHidup(e.target.value)}/>
        </div>
      </div>

      <div className="row align-items-center">
        <div className="col-md-5 label-container"><p className="label">DP:</p></div>
        <div className="col-md-7">
          <input type='text' id='dp' onChange={(e) => setDp(e.target.value)}/>
        </div>
      </div>

      <div className="row align-items-center">
        <div className="col-md-5 label-container"><p className="label">Max. harga:</p></div>
        <div className="col-md-7">
          <input type='text' id='maxHarga'onChange={(e) => setMaxHarga(e.target.value)}/>
        </div>
      </div>

      <div className="row align-items-center">
        <div className="col-md-5 label-container"><p className="label">Jenis motor:</p></div>
        <div className="col-md-7">
          <select name="jenis" onChange={(e) => setJenis(e.target.value)}>
          <option value="scooter">Scooter</option>
          <option value="sport">Sport</option>
          <option value="offroad">Offroad</option>
          <option value="cruiser">Cruiser</option>
        </select>
        </div>
      </div>

      <div className="row">
        <Button className="btn-submit" text = "submit" type="button" value="Submit" handler = {() => props.handler({gaji, biayaHidup, jenis, maxHarga, dp})}/>
      </div>
      
    </form>
  );
}

export default Form;

