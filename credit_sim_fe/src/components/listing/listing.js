// @flow

import React, { useEffect, useState } from 'react';
import './listing.css';
import Motorcycle from '../../type';
import Button from '../button/button';

const Listing = function (props: {list: Array<Motorcycle>}) {
    // default value of empty row
    const [list, setList] = useState([{id:1,price: 10, type:"scooter",brand: "yamaha"}]);
    var rowCounter = 0;

    useEffect(() => {
        setList(props.list);
        console.log("LIST", list);
    }, [props.list]);

    return (
       <div className="table-list">
        <table className="table">
            <thead>
                <tr>
                    <th scope='col'>No.</th>
                    <th scope='col'>Nama Motor</th>
                    <th scope='col'>Jenis</th>
                    <th scope='col'>Harga</th>
                    <th scope='col'>Cicilan</th>
                    <th scope='col'>Periode</th>
                </tr>
            </thead>
            <tbody className='motorcycle-result'>
            {list.map((item) => (
                <tr key={item.id}>
                    <th scope='row'>{rowCounter = rowCounter + 1}</th>
                    <td>{item.motor ? item.motor.nama : '-' }</td>
                    <td>{item.motor ? item.motor.jenis : '-'}</td>
                    <td>{item.motor ? item.motor.harga : '-' }</td>
                    <td>{item.cicilan ? item.cicilan : 'LUNAS' }</td>
                    <td>{item.bulan  ? item.bulan : '-' } Bulan</td>

                </tr>
            ))}
                
            </tbody>
        </table>  
       </div>
    );
}

export default Listing;