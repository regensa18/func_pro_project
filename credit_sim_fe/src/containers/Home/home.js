// @flow
import React, { useEffect, useState } from "react";
import "./home.css";

import Form from '../../components/form/form';
import Listing from "../../components/listing/listing";

const BASE_URL = "https://backend-haskell.herokuapp.com/motor?"
var QUERY_URL = ""
const CORS_URL = "https://cors-anywhere-pemfung.herokuapp.com/"

const Home = function () {
    var [query, setQuery] = useState({gaji: '', biayaHidup: '', jenis: '', maxHarga: '', dp: ''});
    var [result, setResult] = useState([]);

    function handleSubmit(queryFromForm) {
        setQuery(queryFromForm);
    };

    function fetchApi() {
        //build query
        QUERY_URL = "gaji="+query.gaji+"&biayaHidup="+query.biayaHidup+"&jenis="+query.jenis+"&maxHarga="+query.maxHarga+"&dp="+query.dp+"&_accept=application/json";
        
        var API_URL = BASE_URL + QUERY_URL;

      let response = fetch(CORS_URL + API_URL, {
                                method: 'GET'
                            })
                            .then(response => {
                                console.log("API URI", API_URL);
                                return response.json()
                            })
                            .then(data => {
                                console.log("DATA", data); // data -> array
                                setResult(data); // udah ngubah result?
                                console.log("RESULT STATE:", result);
                            })
                            .catch(err => {
                                console.log("OOPS! Error found. " + err)
                            })
    }

    useEffect(() => {
        console.log(`The latest RESULT is`, result);
        console.log("With query:", query);
        fetchApi();
        console.log(`After FETCH`, result);
    }, [query]);

    return (
        <div>
            <Form handler={handleSubmit}/>
            
            <Listing list={result}/>
        </div>
        
    );
}   

export default Home;