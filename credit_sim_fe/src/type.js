type Motorcycle = {
    id: number, 
    price: number,
    type: string,
    brand: string
};